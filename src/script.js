const container = document.querySelector(".container")
const courses = [
    { name: "How to begin with JavaScript", image: "src/images/js.png" },
    { name: "Grids using flex in CSS", image: "src/images/css.png" },
    { name: "React tutorials", image: "src/images/react.png" },
]

const displayCourses = () => {
    let output = ""
    courses.forEach(
        ({ name, image }) =>
            (output += `
              <div class="card">
                <img class="card--avatar" src=${image} />
                <h1 class="card--title">${name}</h1>
                <a class="card--link" href="#">Browse course</a>
              </div>
              `)
    )
    container.innerHTML = output
}

if ("serviceWorker" in navigator) {
    window.addEventListener("load", function () {
        navigator.serviceWorker
            .register("/service-worker.js")
            .then(res => console.log("service worker registered"))
            .catch(err => console.log("service worker not registered", err))
    })
}

document.addEventListener("DOMContentLoaded", displayCourses)
const CACHE = "devshare-precache";
const precacheFiles = [
    "/",
    "/index.html",
    "/src/style.css",
    "/src/script.js",
    "/images/css.png",
    "/images/js.png",
    "/images/react.png",
];

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
        caches.open(CACHE).then(cache => {
            cache.addAll(precacheFiles)
        })
    )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(res => {
            return res || fetch(fetchEvent.request)
        })
    )
})